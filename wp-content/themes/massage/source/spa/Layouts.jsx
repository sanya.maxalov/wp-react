import React, { Component } from 'react';
import { BrowserRouter } from "react-router-dom";

import Header   from "./Header/Header";
import Footer   from "./Footer/Footer";
import Routers from "./Routers";

export default class Layouts extends Component{
    render(){
        return(
            <BrowserRouter>
                <div className={'content'}>
                    <Header />
                    <Routers />
                    <Footer />
                </div>
            </BrowserRouter>
        );
    }
}
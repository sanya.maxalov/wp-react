import React, { Component } from 'react';

export class LoginHeader extends Component{
    render(){
        return (false) ? LoginHeader.login() : LoginHeader.notLogin();
    }
    static login(){
        return(
            <div className={'header-login_login'}>
                login
            </div>
        );
    }
    static notLogin(){
        return(
            <div className={'header-login_not-login'}>
                <p>
                    Вход / Регистрация
                </p>
            </div>
        );
    }
}
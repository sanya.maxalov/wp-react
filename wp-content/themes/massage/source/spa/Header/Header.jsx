import React           from "react";
import { Link }        from 'react-router-dom';
import header          from "./header.css";
import { LoginHeader } from "./top/login/LoginHeader";

export default class Header extends React.Component{
    render(){
        return(
            <div className={"header"}>
                <div className={"top"}>
                    <div className={"container"}>
                        <ul className={'top__list-contacts'}>
                            <li className={'top__item-contacts'}>
                                <a className={'top__link-contacts'} href="#">
                                    +7 (495) 229-81-25
                                </a>
                            </li>
                            <li className={'top__item-contacts'}>
                                ул. Мясницкая, д 8/2, стр. 1 (м. Лубянка)
                            </li>
                        </ul>
                        <LoginHeader/>
                    </div>
                </div>
                <div className={"bottom"}>
                    <div className={"container"}>
                        <ul>
                            <li>
                                <Link to='/'>Главная</Link>
                            </li>
                            <li>
                                <Link to='/contacts'>Контакты</Link>
                            </li>
                        </ul>
                        <ul className={'bottom__list-social'}>
                            <li className={'bottom__item-social'}>
                                <a href="" className={'bottom__link-social'}>
                                    <img src="./images/inst.svg" width={'30px'}/>
                                </a>
                            </li>
                            <li className={'bottom__item-social'}>
                                <a href="" className={'bottom__link-social'}>
                                    <img src="./images/vk.svg" width={'30px'}/>
                                </a>
                            </li>
                            <li className={'bottom__item-social'}>
                                <a href="" className={'bottom__link-social'}>
                                    <img src="./images/facebook.svg" width={'30px'}/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
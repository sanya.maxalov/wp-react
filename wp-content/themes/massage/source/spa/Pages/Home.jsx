import React, { Component } from "react";
import path from "path";

import EnterMassage from "../common.blocks/enter-massage/EnterMassage";
import ListGirlHome from "../common.blocks/list-girl-home/ListGirlHome";

export default class Home extends Component {
    render(){
        return(
            <section className={'home-page'}>
                <div className={'banner-home'}>
                    <img src={ path.join('./images/banner.jpg') } width={'100%'} />
                </div>
                <EnterMassage />
                <ListGirlHome />
            </section>
        );
    }
}
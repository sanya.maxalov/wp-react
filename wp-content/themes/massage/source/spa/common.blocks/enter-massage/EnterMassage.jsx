import React, { Component } from 'react';

export default class EnterMassage extends Component {
    render(){
        return(
            <div className={'enter-massage'}>
                <div className={'container'}>
                    <div className={'wrappper'}>
                        <form className={'form'} action="">
                            <div className={'form__name'}>
                                <p>Имя</p>
                                <input type="text"/>
                            </div>
                            <div className={'form__tel'}>
                                <p>Телефон</p>
                                <input type="tel"/>
                            </div>
                            <div className={'form__date'}>
                                <p>Дата</p>
                                <input type="date"/>
                            </div>
                            <div className={'form__time'}>
                                <p>Время</p>
                                <input type="text"/>
                            </div>
                            <input type="submit" value={'Записаться на массаж'}/>
                        </form>
                    </div>    
                </div>
            </div>
        );
    }
}
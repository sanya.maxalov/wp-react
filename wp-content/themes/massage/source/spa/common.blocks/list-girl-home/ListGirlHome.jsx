import React, { Component } from "react";
import path from "path";

import "./list-girl-home.css";

export default class ListGirlHome extends Component {
    render(){
        return(
        <div className={'list-girl-home'}>
            <div className={'container'}>
                <div className={'title'}>
                    <p className={'title__wrapper'}>
                        Девушки
                    </p>
                </div>
                <ul className={'girls'}>
                    <li className={'girls__item'}>
                        <p className={'girls__name'}>
                            Девушка
                        </p>
                        <img src={ path.join('./images/girl.jpg') } width={'100%'} />
                        { ListGirlHome.ListParams() }
                    </li>
                </ul>
            </div>
        </div>
        );
    }

    static ListParams(){
        return(
            <ul className={'params'}>
                <li className={'params__item'}>
                    Возраст: 20
                </li>
                <li className={'params__item'}>
                    Грудь: 3
                </li>
                <li className={'params__item'}>
                    Силикон: Нет
                </li>
            </ul>
        );
    }
}
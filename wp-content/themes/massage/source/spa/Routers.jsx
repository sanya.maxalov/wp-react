import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Contacts from "./Pages/Contacts";
import Home from "./Pages/Home";

export default class Routers extends Component{
    render(){
        return(
            <Switch>
                <Route exact path='/'
                       component={Home}/>

                <Route path='/contacts'
                       component={Contacts} />
            </Switch>
        );
    }
}
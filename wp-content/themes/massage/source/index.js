import React    from 'react';
import ReactDOM from 'react-dom';
import Layouts from "./spa/Layouts";

/**
 * base Style
 */
import "./style.css";

ReactDOM.render((
    <Layouts />
    ), document.getElementById('root')
);
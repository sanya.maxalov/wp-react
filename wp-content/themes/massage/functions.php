<?php

function remove_menus(){
    remove_menu_page( 'edit.php' );
}

add_action( 'admin_menu', 'remove_menus' );


add_theme_support( 'post-thumbnails' );

function wpng_register_required_plugins() {
    $plugins = array(
        array(
            'name'               => 'WordPress REST API (Version 2)',
            'slug'               => 'rest-api',
            'source'             => get_stylesheet_directory() . '/plugins/rest-api.2.0-beta15.zip',
            'required'           => true,
            'version'            => '',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
            'is_callable'        => '',
        ),
    );
    $config = array();
    tgmpa( $plugins, $config );
}

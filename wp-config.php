<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wp-react');

/** Имя пользователя MySQL */
define('DB_USER', 'homestead');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'secret');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '* v@KkDIvS= +5o+oT-!-B{ia7JdDFUSzE%M~2ciePVhNCz<r%gkOsgu+>R O71_');
define('SECURE_AUTH_KEY',  'M72d1I.J2AG}jZ)E>]1U2*{ t4~US{Yl@/ty~E:AqZq(AqwlPZx;%sGbV#H8IWnn');
define('LOGGED_IN_KEY',    '^+f6xmA,LDoDwPfYy(8H2FNe`(`(?a7VVqe)U^W-QpYF3pMOqxSd^Kq$36t):;9b');
define('NONCE_KEY',        '-j#yO1;QKy=R<F^bg.4}See2(m6[B:u#]0RGF||zl _^N}vIM(s%&CZ~x0X)W<+I');
define('AUTH_SALT',        'Gx!F/b$IByGxk?>oxL2!^9s(I0~~ncyP>&rV0W~#y;n61%NCQ[&2<albpJ:>1juQ');
define('SECURE_AUTH_SALT', '.-8C{UV:tBJwFm6[`BWH7t8  }kYK|_v4#.,!(kOP3IT_jcRb|qb(izg;Z`vFGLu');
define('LOGGED_IN_SALT',   ' vOI~I/X=a=F!^lb>WXOQ?P7gw=Q.$Bb E7yy8P@F4w?{.1I3K.x+ W[ZwEHwSvP');
define('NONCE_SALT',       '5V{1]#>Sd Ebah&Fv.}JNuO.UDX9aF]TGTq1gL. TQO^Xd 2*o~GQO14m Za]]ea');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
